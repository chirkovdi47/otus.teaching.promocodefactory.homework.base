﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employeess = await _employeeRepository.GetAllAsync();

            var employeesModelList = employeess.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("Create")]
        public async Task<ActionResult> CreateNewEmployeeAsync(string email, string firstName, string lastName, List<string> roleName, int promocodes)
        {

            var roles = await _rolesRepository.GetAllAsync();

            var role = roles.Where(r => roleName.Any(n => n == r.Name)).ToList();

            if (role.Count <= 0)
                return NotFound("Такой роли не существует");

            var employeesModelList = new Employee
            {
                Id = Guid.NewGuid(),
                Email = email,
                FirstName = firstName,
                LastName = lastName,
                Roles = role,
                AppliedPromocodesCount = promocodes
            };

            await _employeeRepository.CreateAsync(employeesModelList);

            return Ok("Новый сотрудник создан");
        }

        /// <summary>
        /// Удалить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("Delete/{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> DeleteEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound("Сотрудника с таким id не существует");

            await _employeeRepository.DeleteByIdAsync(id);

            return Ok("Данные удалены");
        }

        /// <summary>
        /// Обновить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpPost("Update/{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployeeByIdAsync(Guid id, string email, string firstName, string lastName, List<string> roleName, int promocodes)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound("Сотрудника с таким id не существует");

            var roles = await _rolesRepository.GetAllAsync();

            var role = roles.Where(r => roleName.Any(n => n == r.Name)).ToList();

            if (role.Count <= 0)
                return NotFound("Такой роли не существует");

            var employeesModelList = new Employee
            {
                Id = id,
                Email = email,
                FirstName = firstName,
                LastName = lastName,
                Roles = role,
                AppliedPromocodesCount = promocodes
            };

            await _employeeRepository.UpdateByIdAsync(id, employeesModelList);

            return Ok("Данные обновлены");
        }
    }
}
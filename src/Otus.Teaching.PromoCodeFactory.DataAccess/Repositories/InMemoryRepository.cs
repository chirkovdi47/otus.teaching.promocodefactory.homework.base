﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateAsync(T employeesModelList)
        {
            var data = Data.Append(employeesModelList);
            Data = data;
            return Task.FromResult(employeesModelList);
        }

        public Task<Guid> DeleteByIdAsync(Guid id)
        {
            var data = Data.Where(x => x.Id != id);
            Data = data;
            return Task.FromResult(id);
        }

        public Task<T> UpdateByIdAsync(Guid id, T employeesModelList)
        {
            var data = Data.Where(x => x.Id != id);
            Data = data.Append(employeesModelList);
            return Task.FromResult(employeesModelList);
        }
    }
}